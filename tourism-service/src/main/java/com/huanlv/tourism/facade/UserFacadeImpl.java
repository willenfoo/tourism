package com.huanlv.tourism.facade;

import com.huanlv.tourism.service.impl.v2.UserServiceImpl;
import com.huanlv.tourism.service.v2.UserService;

public class UserFacadeImpl implements UserFacade {

	private UserService userService = new UserServiceImpl();
	
	public void find() {
		userService.find();
	}

}
