package com.huanlv.tourism.service.impl;

import com.huanlv.tourism.service.ProductService;
import com.huanlv.tourism.service.UserService;

public class ProductServiceImpl implements ProductService {

	private UserService userService = new UserServiceImpl();
	
	public void find() {
		userService.find();
		System.out.println("Product find");
	}

	public void update() {
		userService.update();
		System.out.println("Product update");
	}

	
}
