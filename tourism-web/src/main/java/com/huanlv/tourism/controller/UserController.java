package com.huanlv.tourism.controller;

import com.huanlv.tourism.service.ProductService;
import com.huanlv.tourism.service.UserService;
import com.huanlv.tourism.service.impl.ProductServiceImpl;
import com.huanlv.tourism.service.impl.UserServiceImpl;

public class UserController {

	private UserService userService = new UserServiceImpl();
	
	private ProductService productService = new ProductServiceImpl();
	
	public void find() {
		userService.find();
		productService.find();
	}
	
	
}
